using System.Text;

namespace Altran.Configuration
{
    public class AuthenticationOptions
    {
        public AuthenticationOptions()
        {

        }

        /// <summary>
        /// Get SecretKey to JWToken
        /// </summary>
        public string SecretKey { get; set; }
        
        /// <summary>
        /// Get SecretKey to JWToken in byte
        /// </summary>
        public byte[] SecretKeyInByte 
        { 
            get 
            {
                return Encoding.ASCII.GetBytes(this.SecretKey);
            }
        }
    }
}