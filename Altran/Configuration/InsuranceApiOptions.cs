using System.Text;

namespace Altran.Configuration
{
    public class InsuranceApiOptions
    {
        public InsuranceApiOptions()
        {

        }

        /// <summary>
        /// Get Api Insurance company host
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Get Api Insurance company Client method
        /// </summary>
        public string Clients { get; set; }

        /// <summary>
        /// Get Api Insurance company Policies method
        /// </summary>
        public string Policies { get; set; }
    }
}