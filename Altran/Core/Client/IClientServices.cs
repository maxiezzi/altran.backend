using System;
using System.Collections.Generic;
using Altran.Models;

namespace Altran.Core
{
    public interface IClientServices
    {
        /// <summary>
        /// get client by id
        /// </summary>
        /// <param name="id">client id</param>
        /// <returns>return <see cref="Client"/></returns>
        /// <exception cref="ClientNotFoundException">Client not found</exception>
        /// <exception cref="ClientFieldRequiredException">Client id is required</exception>
        Client GetClientById(string id);
        
        /// <summary>
        /// get client by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns>return <see cref="Client"/></returns>
        /// <exception cref="ClientNotFoundException">Client not found</exception>
        /// <exception cref="ClientFieldRequiredException">Client id is required</exception>
        Client GetClientByEmail(string email);

        /// <summary>
        /// get client by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>return <see cref="Client"/></returns>
        /// <exception cref="ClientNotFoundException">Client not found</exception>
        /// <exception cref="ClientFieldRequiredException">Client id is required</exception>
        Client GetClientByName(string name);
    }
}