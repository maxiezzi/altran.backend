
namespace Altran.Core.Exception
{
    public class ClientNotFoundException : System.Exception
    {
        public ClientNotFoundException(string id) : base($"The client {id} was not found."){}
    }
}