namespace Altran.Core.Exception
{
    public class ClientFieldRequiredException : System.Exception
    {
        public ClientFieldRequiredException(string field) : base($"The field {field} is required."){}
    }
}