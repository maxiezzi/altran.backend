using System.Collections.Generic;
using Altran.BusinessServices;
using Altran.Configuration;
using Altran.Core.Exception;
using Altran.Models;
using Altran.Utils;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RestSharp;

namespace Altran.Core
{
    public class ClientServices : IClientServices
    {
        private readonly ILogs _logs;
        private readonly ILogger _logger;
        private readonly IApiEnsureServices _apiEnsureServices;

        public ClientServices(IApiEnsureServices apiEnsureServices, ILoggerFactory loggerFactory, ILogs logs)
        {
            this._logger = loggerFactory.CreateLogger<ClientServices>();
            this._logs = logs;
            this._apiEnsureServices = apiEnsureServices;
        }
        
        /// <summary>
        /// get client by id
        /// </summary>
        /// <param name="id">client id</param>
        /// <returns>return <see cref="Client"/></returns>
        /// <exception cref="ClientNotFoundException">Client not found</exception>
        /// <exception cref="ClientFieldRequiredException">Client id is required</exception>
        public virtual Client GetClientById(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new ClientFieldRequiredException("id");
            }

            List<Client> clients = _apiEnsureServices.GetClients();
            Client client = clients.Find(client => client.Id.Equals(id));
            
            if (client == null)
            {
                throw new ClientNotFoundException(id);
            }

            return client;
        }

        /// <summary>
        /// get client by email
        /// </summary>
        /// <param name="email"></param>
        /// <returns>return <see cref="Client"/></returns>
        /// <exception cref="ClientNotFoundException">Client not found</exception>
        /// <exception cref="ClientFieldRequiredException">Client id is required</exception>
        public virtual Client GetClientByEmail(string email)
        {
            if (string.IsNullOrEmpty(email))
            {
                throw new ClientFieldRequiredException("email");
            }

            List<Client> clients = _apiEnsureServices.GetClients();
            Client client = clients.Find(client => client.Email.Equals(email, System.StringComparison.InvariantCultureIgnoreCase));
            
            if (client == null)
            {
                throw new ClientNotFoundException(email);
            }

            return client;
        }

        /// <summary>
        /// get client by name
        /// </summary>
        /// <param name="name"></param>
        /// <returns>return <see cref="Client"/></returns>
        /// <exception cref="ClientNotFoundException">Client not found</exception>
        /// <exception cref="ClientFieldRequiredException">Client id is required</exception>
        public virtual Client GetClientByName(string name)
        {
            if (string.IsNullOrEmpty(name))
            {
                throw new ClientFieldRequiredException("name");
            }

            List<Client> clients = _apiEnsureServices.GetClients();
            Client client = clients.Find(client => client.Name.Equals(name, System.StringComparison.InvariantCultureIgnoreCase));
            
            if (client == null)
            {
                throw new ClientNotFoundException(name);
            }

            return client;
        }
    }
}