using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Altran.BusinessServices;
using Altran.Configuration;
using Altran.Core.Exception;
using Altran.Models;
using Altran.Utils;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using RestSharp;

namespace Altran.Core
{
    public class AuthenticationServices : IAuthenticationServices
    {
        private readonly ILogs _logs;
        private readonly ILogger _logger;
        private readonly IClientServices _clientServices;
        private readonly AuthenticationOptions _authenticationOptions;

        public AuthenticationServices(IClientServices clientServices, IOptions<AuthenticationOptions> authenticationOptions, ILoggerFactory loggerFactory, ILogs logs)
        {
            this._logger = loggerFactory.CreateLogger<AuthenticationServices>();
            this._logs = logs;
            this._clientServices = clientServices;
            this._authenticationOptions = authenticationOptions.Value;
        }
        public virtual string Login(string email)
        {
            Client client = _clientServices.GetClientByEmail(email);

            // add claims
            ClaimsIdentity claims = new ClaimsIdentity();
            claims.AddClaim(new Claim(ClaimTypes.NameIdentifier, client.Id));
            claims.AddClaim(new Claim(ClaimTypes.Email, client.Name));
            claims.AddClaim(new Claim(ClaimTypes.Role, client.Role.ToLower()));
            claims.AddClaim(new Claim(ClaimTypes.Name, client.Name));

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = claims,
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(_authenticationOptions.SecretKeyInByte), SecurityAlgorithms.HmacSha256Signature)
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var createdToken = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(createdToken);
        }
    }
}