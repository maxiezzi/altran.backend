using System;
using System.Collections.Generic;
using Altran.Models;

namespace Altran.Core
{
    public interface IAuthenticationServices
    {
        string Login(string email);
    }
}