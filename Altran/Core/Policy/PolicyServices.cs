using System;
using System.Collections.Generic;
using Altran.Configuration;
using Altran.Controllers;
using Altran.Models;
using Altran.Utils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Altran.BusinessServices;
using Altran.Core.Exception;

namespace Altran.Core
{
    public class PolicyServices : IPolicyServices
    {
        private readonly ILogs _logs;
        private readonly ILogger _logger;
        private readonly IApiEnsureServices _apiEnsureServices;

        public PolicyServices(IApiEnsureServices apiEnsureServices, ILoggerFactory loggerFactory, ILogs logs)
        {
            this._logger = loggerFactory.CreateLogger<PolicyServices>();
            this._apiEnsureServices = apiEnsureServices;
            this._logs = logs;
        }

        // <summary>
        /// get one policy by id
        /// </summary>
        /// <returns>return list of <see cref="Policy"/></returns>
        /// <exception cref="PolicyNotFoundException">Client not found</exception>
        /// <exception cref="PolicyFieldRequiredException">Client id is required</exception>
        public virtual Policy GetPolicyById(string id)
        {
            if (string.IsNullOrEmpty(id))
            {
                throw new PolicyFieldRequiredException("id");
            }

            List<Policy> policies = _apiEnsureServices.GetPolicies();
            Policy policy = policies.Find(policy => policy.Id.Equals(id));
            
            if (policy == null)
            {
                throw new PolicyNotFoundException(id, "id");
            }

            return policy;
        }

        /// <summary>
        /// get all policies by clientid
        /// </summary>
        /// <returns>return list of <see cref="Policy"/></returns>
        /// <exception cref="PolicyNotFoundException">Client not found</exception>
        /// <exception cref="PolicyFieldRequiredException">Client id is required</exception>
        public virtual List<Policy> GetPoliciesByClientId(string clientId)
        {
            if (string.IsNullOrEmpty(clientId))
            {
                throw new PolicyFieldRequiredException("clientId");
            }

            List<Policy> policies = _apiEnsureServices.GetPolicies();
            List<Policy> policiesResult = policies.FindAll(policy => policy.ClientId.Equals(clientId));
            
            if (policiesResult == null || policiesResult.Count == 0)
            {
                throw new PolicyNotFoundException(clientId, "clientId");
            }

            return policiesResult;
        }
    }
}