
namespace Altran.Core.Exception
{
    public class PolicyNotFoundException : System.Exception
    {
        public PolicyNotFoundException(string value, string field) : base($"The policy was not found. Field ${field}$ value ${value}$"){}
    }
}