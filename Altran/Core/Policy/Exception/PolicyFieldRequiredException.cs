
namespace Altran.Core.Exception
{
    public class PolicyFieldRequiredException : System.Exception
    {
        public PolicyFieldRequiredException(string field) : base($"The field {field} is required."){}
    }
}