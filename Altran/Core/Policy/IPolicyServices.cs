using System;
using System.Collections.Generic;
using Altran.Models;
using Altran.Core.Exception;

namespace Altran.Core
{
    public interface IPolicyServices
    {

        /// <summary>
        /// get all policies by clientid
        /// </summary>
        /// <returns>return list of <see cref="Policy"/></returns>
        /// <exception cref="PolicyNotFoundException">Client not found</exception>
        /// <exception cref="PolicyFieldRequiredException">Client id is required</exception>
        List<Policy> GetPoliciesByClientId(string clientId);

        /// <summary>
        /// get one policy by id
        /// </summary>
        /// <returns>return list of <see cref="Policy"/></returns>
        /// <exception cref="PolicyNotFoundException">Client not found</exception>
        /// <exception cref="PolicyFieldRequiredException">Client id is required</exception>
        Policy GetPolicyById(string id);
    }
}