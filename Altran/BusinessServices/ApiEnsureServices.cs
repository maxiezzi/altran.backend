using System;
using System.Collections.Generic;
using Altran.Configuration;
using Altran.Controllers;
using Altran.Core;
using Altran.Core.Exception;
using Altran.Models;
using Altran.Utils;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using RestSharp;


namespace Altran.BusinessServices
{
    public class ApiEnsureServices : IApiEnsureServices
    {
        private readonly ILogs _logs;
        private readonly ILogger _logger;
        private readonly InsuranceApiOptions _insuranceApiOptions;

        public ApiEnsureServices(IOptions<InsuranceApiOptions> insuranceApiOptions, ILoggerFactory loggerFactory, ILogs logs)
        {
            this._logger = loggerFactory.CreateLogger<PolicyServices>();
            this._insuranceApiOptions = insuranceApiOptions.Value;
            this._logs = logs;
        }

        /// <summary>
        /// get all clients of client business
        /// </summary>
        /// <returns>return list of <see cref="Client"/></returns>
        public virtual List<Client> GetClients()
        {
            try
            {
                var client = new RestClient(_insuranceApiOptions.Host);
                var request = new RestRequest(_insuranceApiOptions.Clients, Method.GET);

                IRestResponse<ClientsResponse> response = client.Execute<ClientsResponse>(request);
                return response.Data.Clients as List<Client>;
            }
            catch (Exception ex)
            {
                _logs.GuardarLogs(ex, _logger, this.GetType());
                throw new ApiEnsureException();
            }
        }

        /// <summary>
        /// get all policies of client business
        /// </summary>
        /// <returns>return list of <see cref="Policy"/></returns>
        public virtual List<Policy> GetPolicies()
        {
            try
            {
                var client = new RestClient(this._insuranceApiOptions.Host);
                var request = new RestRequest(this._insuranceApiOptions.Policies, Method.GET);

                IRestResponse<PoliciesResponse> response = client.Execute<PoliciesResponse>(request);
                return response.Data.Policies as List<Policy>;
            }
            catch (Exception ex)
            {
                _logs.GuardarLogs(ex, _logger, this.GetType());
                throw new ApiEnsureException();
            }
        }
    }
}