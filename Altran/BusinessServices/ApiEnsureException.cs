
namespace Altran.BusinessServices
{
    public class ApiEnsureException : System.Exception
    {
        public ApiEnsureException() : base($"The ensure api was failed."){}
    }
}