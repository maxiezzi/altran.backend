using System.Collections.Generic;
using Altran.Models;

namespace Altran.BusinessServices
{
    public interface IApiEnsureServices
    {
        /// <summary>
        /// get all clients of client business
        /// </summary>
        /// <returns>return list of <see cref="Client"/></returns>
        List<Client> GetClients();

        /// <summary>
        /// get all policies of client business
        /// </summary>
        /// <returns>return list of <see cref="Policy"/></returns>
        List<Policy> GetPolicies();

    }
}