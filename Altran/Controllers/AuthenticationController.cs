using System;
using System.Dynamic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Altran.BusinessServices;
using Altran.Configuration;
using Altran.Core;
using Altran.Core.Exception;
using Altran.Models;
using Altran.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json;

namespace Altran.Controllers
{
    [Route("api/[controller]")]
    public class AuthenticationController : Controller
    {
        private readonly ILogger _logger;
        private readonly AuthenticationOptions _authenticationOptions;
        private readonly IAuthenticationServices _authenticationServices;
        private readonly ILogs _logs;
        public AuthenticationController(ILoggerFactory loggerFactory, IOptions<AuthenticationOptions> authenticationOptions, IAuthenticationServices authenticationServices, ILogs logs)
        {
            _logger = loggerFactory.CreateLogger<AuthenticationController>();
            _authenticationOptions = authenticationOptions.Value;
            _authenticationServices = authenticationServices;
            _logs = logs;
        }

        [HttpGet]
        [Route("[action]")]
        [Authorize(Roles = "admin,user")]
        public ActionResult<bool> IsLogin()
        {
            return Ok(true);
        }

        [HttpPost]
        [Route("login")]
        /// <summary>
        /// Generate jwt token if user is valid
        /// </summary>
        /// <param name="email">Email with login user</param>
        /// <returns>Error or Token in <see cref="AuthenticationResponse" /></returns>
        [AllowAnonymous]
        public ActionResult Login([FromForm] string email)
        {
            dynamic response = new ExpandoObject();
            try
            {
                response.token = _authenticationServices.Login(email);
                return Ok(JsonConvert.SerializeObject(response));
            }            
            catch (ClientNotFoundException)
            {
                response.Error = "Client not found.";
                return NotFound(JsonConvert.SerializeObject(response));
            }
            catch (ClientFieldRequiredException)
            {
                response.Error = "Client email is required";
                return NotFound(JsonConvert.SerializeObject(response));
            }
            catch (Exception ex)
            {
                _logs.GuardarLogs(ex, _logger, this.GetType());
                response.Error = "Fatal error";
                return StatusCode((int)503, JsonConvert.SerializeObject(response));
            }
        }
    }
}