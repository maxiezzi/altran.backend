﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using Altran.BusinessServices;
using Altran.Core;
using Altran.Core.Exception;
using Altran.Models;
using Altran.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Altran.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class ClientController : ControllerBase
    {
        private readonly ILogger _logger;
        private readonly IClientServices _clientServices;
        private readonly IPolicyServices _policyServices;
        private readonly ILogs _logs;

        public ClientController(ILoggerFactory loggerFactory, IClientServices clientServices, IPolicyServices policyServices, ILogs logs)
        {
            _logger = loggerFactory.CreateLogger<ClientController>();
            _clientServices = clientServices;
            _policyServices = policyServices;
            _logs = logs;
        }

        /// <summary>
        /// Get one client by id
        /// </summary>
        /// <param name="id">the clien's id</param>
        /// <returns>return one <see cref="Client"/> or null</returns>
        // GET: api/client/5
        [HttpGet("{id}")]
        [Authorize(Roles = "admin,user")]
        public ActionResult GetById(string id)
        {
            dynamic response = new ExpandoObject();
            try
            {
                response.Client = _clientServices.GetClientById(id);;
                return Ok(JsonConvert.SerializeObject(response));
            }            
            catch (ClientNotFoundException)
            {
                response.Error = "Client not found.";
                return NotFound(JsonConvert.SerializeObject(response));
            }
            catch (ClientFieldRequiredException)
            {
                response.Error = "Client id is required";
                return NotFound(JsonConvert.SerializeObject(response));
            }
            catch (Exception ex)
            {
                _logs.GuardarLogs(ex, _logger, this.GetType());
                response.Error = "Fatal error";
                return StatusCode((int)503, JsonConvert.SerializeObject(response));
            }
        }

        /// <summary>
        /// Get One client by name
        /// </summary>
        /// <param name="name">the client's name</param>
        /// <returns>return one <see cref="Client"/> or null</returns>
        [HttpGet("name/{name}")]
        [Authorize(Roles = "admin,user")]
        public ActionResult GetByName(string name)
        {
            dynamic response = new ExpandoObject();
            try
            {
                response.Client = _clientServices.GetClientByName(name);;
                return Ok(JsonConvert.SerializeObject(response));
            }
            catch (ClientNotFoundException)
            {
                response.Error = "Client not found.";
                return NotFound(JsonConvert.SerializeObject(response));
            }
            catch (ClientFieldRequiredException)
            {
                response.Error = "Name is required";
                return NotFound(JsonConvert.SerializeObject(response));
            }
            catch (Exception ex)
            {
                _logs.GuardarLogs(ex, _logger, this.GetType());
                response.Error = "Fatal error";
                return StatusCode((int)503, JsonConvert.SerializeObject(response));
            }

        }

        /// <summary>
        /// Get one Client by Policy number
        /// </summary>
        /// <param name="policyId">number of policy</param>
        /// <returns>return one <see cref="Client"/> or null</returns>
        [HttpGet("policy/{policyId}")]
        [Authorize(Roles = "admin")]
        public ActionResult GetByPolicyNumber(string policyId)
        {
            dynamic response = new ExpandoObject();
            try
            {
                Policy policy = _policyServices.GetPolicyById(policyId);
                response.Client = _clientServices.GetClientById(policy.ClientId);
                return Ok(JsonConvert.SerializeObject(response));
            }
            catch (PolicyFieldRequiredException)
            {
                response.Error = "Policy is required";
                return NotFound(JsonConvert.SerializeObject(response));
            }
            catch (PolicyNotFoundException)
            {
                response.Error = "Policy not found.";
                return NotFound(JsonConvert.SerializeObject(response));
            }
            catch (ClientNotFoundException)
            {
                response.Error = "Client not found.";
                return NotFound(JsonConvert.SerializeObject(response));
            }
            catch (Exception ex)
            {
                _logs.GuardarLogs(ex, _logger, this.GetType());
                response.Error = "Fatal error";
                return StatusCode((int)503, JsonConvert.SerializeObject(response));
            }
        }
    }
}
