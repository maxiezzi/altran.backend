using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using Altran.BusinessServices;
using Altran.Core;
using Altran.Core.Exception;
using Altran.Models;
using Altran.Utils;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;

namespace Altran.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class PolicyController : ControllerBase
    {
        #region props
        private readonly ILogger _logger;
        private readonly IClientServices _clientServices;
        private readonly IPolicyServices _policyServices;
        private readonly ILogs _logs;
        #endregion
        public PolicyController(ILoggerFactory loggerFactory, IClientServices clientServices, IPolicyServices policyServices, ILogs logs)
        {
            _logger = loggerFactory.CreateLogger<PolicyController>();
            _clientServices = clientServices;
            _policyServices = policyServices;
            _logs = logs;
        }

        /// <summary>
        /// Get all Policy by clients's name
        /// </summary>
        /// <param name="name">the clien's name</param>
        /// <returns>return all <see cref="Policy"/> or null</returns>
        [HttpGet("client/name/{name}")]
        [Authorize(Roles = "admin")]
        public ActionResult GetAllByUserName(string name)
        {
            dynamic response = new ExpandoObject();
            try
            {
                Client client = _clientServices.GetClientByName(name);
                response.Policies = _policyServices.GetPoliciesByClientId(client.Id);;
                return Ok(JsonConvert.SerializeObject(response));
            }
            catch (PolicyNotFoundException)
            {
                response.Error = "Policy not found";
                return NotFound(JsonConvert.SerializeObject(response));
            }
            catch (ClientNotFoundException)
            {
                response.Error = "Client not found.";
                return NotFound(JsonConvert.SerializeObject(response));
            }
            catch (ClientFieldRequiredException)
            {
                response.Error = "Client name is required";
                return NotFound(JsonConvert.SerializeObject(response));
            }
            catch (Exception ex)
            {
                _logs.GuardarLogs(ex, _logger, this.GetType());
                response.Error = "Fatal error";
                return StatusCode((int)503, JsonConvert.SerializeObject(response));
            }
        }
    }
}
