
using System.Collections.Generic;

namespace Altran.Models
{
    public class ClientsResponse
    {
        public List<Client> Clients { get; set; }
    }
}