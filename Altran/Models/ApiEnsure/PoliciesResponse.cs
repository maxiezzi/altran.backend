
using System.Collections.Generic;

namespace Altran.Models
{
    public class PoliciesResponse
    {
        public List<Policy> Policies { get; set; }
    }
}