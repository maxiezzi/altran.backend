using System;

namespace Altran.Models
{
    /// <summary>
    /// The main Policy class.
    /// This class supports polities of insurance company
    /// </summary>
    public class Policy
    {
        /// <summary>
        /// Get or Set Id of polici.
        /// </summary>
        public string Id {get; set;}
        
        /// <summary>
        /// Get or Set amount insured.
        /// </summary>
        public float amountInsured {get; set;}
        
        /// <summary>
        /// Get or Set the declared email.
        /// </summary>
        public string Email {get; set;}
        
        /// <summary>
        /// Get or Set the inception Date.
        /// </summary>
        public DateTime InceptionDate{get;set;}
        
        /// <summary>
        /// Get or Set .
        /// </summary>
        public bool InstallmentPaymeny {get; set;}
        
        /// <summary>
        /// Get or Set id of client <see cref="Client"/>.
        /// </summary>
        public string ClientId {get; set;} 
    }
}