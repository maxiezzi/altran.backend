namespace Altran.Models
{
    /// <summary>
    /// The main Client Class
    /// This class supports clients of insurance company
    /// </summary>
    public class Client
    {
        /// <summary>
        /// Get or Set the client's Id.
        /// </summary> 
        public string Id {get; set;}
        
        /// <summary>
        /// Get or Set the client's Name.
        /// </summary> 
        public string Name {get; set;}

        /// <summary>
        /// Get or Set the client's email.
        /// </summary> 
        public string Email {get; set;}

        /// <summary>
        /// Get or Set Clients Roles
        /// </summary>
        public string Role {get; set;}
    }
}