using System.Diagnostics.CodeAnalysis;
using System.Text;
using Altran.BusinessServices;
using Altran.Configuration;
using Altran.Core;
using Altran.Utils;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.IdentityModel.Tokens;

namespace Altran
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        #region props
        private IConfiguration Configuration { get; }
        private byte[] GetSecretkey() => Encoding.ASCII.GetBytes(Configuration["Authentication:SecretKey"]);
        #endregion

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            //logging
            LoggerFactory LoggerFactory = new LoggerFactory();
            LoggerFactory.AddLog4Net();
            services.AddSingleton<ILoggerFactory>(LoggerFactory);

            //add clients services
            services.AddScoped<IApiEnsureServices, ApiEnsureServices>();

            //add core
            services.AddScoped<IAuthenticationServices, AuthenticationServices>();
            services.AddScoped<IClientServices, ClientServices>();
            services.AddScoped<IPolicyServices, PolicyServices>();
            services.AddScoped<ILogs, Logs>();
            
            //options 
            services.Configure<InsuranceApiOptions>(Configuration.GetSection("Api"));
            services.Configure<AuthenticationOptions>(Configuration.GetSection("Authentication"));

            //authenticacion
            services.AddAuthentication(x =>
                    {
                        x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                        x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                    }).AddJwtBearer(x =>
                    {
                        x.RequireHttpsMetadata = false;
                        x.SaveToken = true;
                        x.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = new SymmetricSecurityKey(GetSecretkey()),
                            ValidateIssuer = false,
                            ValidateAudience = false
                        };
                    });

            services.AddControllers()
                    .AddJsonOptions(options => {
                        options.JsonSerializerOptions.IgnoreNullValues = true;
                    });
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
