using System;
using Microsoft.Extensions.Logging;

namespace Altran.Utils
{
    public class Logs : ILogs
    {
        public void GuardarLogs(Exception ex, ILogger log, Type tipo)
        {
            if (log.IsEnabled(LogLevel.Error))
            {
                log.LogError("\nType: {type} \nMessage: {mensaje} \nInnerException: {inner} \n StackTrace: {trace} ", 
                            tipo.ToString(), 
                            ex.Message,
                            ex.InnerException == null ? "" : ex.InnerException.ToString(),
                            ex.StackTrace == null ? "" : ex.StackTrace);
            }
        }
    }
}