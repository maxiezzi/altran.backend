using System;
using Microsoft.Extensions.Logging;

namespace Altran.Utils
{
    public interface ILogs
    {
        void GuardarLogs(Exception ex, ILogger log, Type tipo);
    }
}