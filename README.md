# Backend code assessment for Altran.

It's a restfull api, development with webapi (.Net core). 

The api have 3 controller 
1. api/Authentication
*  /login (Post): authentication by email
*  /isLogin (Get): is loggin was successful
2. api/Client
*   /{CliendId} (Get): Get one client by id
*   /name/{name} (Get): Get one client by name
*   /policy/{policyId} (Get): Get one client by policy number
*   
3. api/Policy
*   /client/name/{name} (Get): get all policies by user name

You can authentication by email, the method return your bearer token. You can 

Auxiliary libreries
1. JwtBearer
2. RestSharp
3. Log4net
4. Mock


# Requirement
install .Net Core

# To Run the app
In the project directory, go to Altran folder and you must run
```
dotnet run
```