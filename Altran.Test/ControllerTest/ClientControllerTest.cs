using System;
using System.Collections.Generic;
using Altran.BusinessServices;
using Altran.Controllers;
using Altran.Core;
using Altran.Core.Exception;
using Altran.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace Altran.Test.BusinessServicesTest
{
    public class ClientControllerTest
    {
        #region props
        private List<Client> clients = new List<Client>();
        private List<Policy> policies = new List<Policy>();
        private Mock<IClientServices> clientServicesMock {get;}
        private Mock<IPolicyServices> policiesServicesMock {get;}
        private Mock<ILoggerFactory> loggerFactoryMock {get;}
        #endregion props

        public ClientControllerTest()
        {
            policies.Add(new Policy()
            {
                Id = "1",
                Email = "1@gmail.com",
                amountInsured = 12,
                InceptionDate = DateTime.Now,
                InstallmentPaymeny = true,
                ClientId = "client1"
            });

            policies.Add(new Policy()
            {
                Id = "2",
                Email = "1@gmail.com",
                amountInsured = 12,
                InceptionDate = DateTime.Now,
                InstallmentPaymeny = true,
                ClientId = "client1"
            });

            policies.Add(new Policy()
            {
                Id = "3",
                Email = "2@gmail.com",
                amountInsured = 12,
                InceptionDate = DateTime.Now,
                InstallmentPaymeny = true,
                ClientId = "client2"
            });

            policies.Add(new Policy()
            {
                Id = "4",
                Email = "2@gmail.com",
                amountInsured = 12,
                InceptionDate = DateTime.Now,
                InstallmentPaymeny = true,
                ClientId = "client2"
            });

            policies.Add(new Policy()
            {
                Id = "5",
                Email = "3@gmail.com",
                amountInsured = 12,
                InceptionDate = DateTime.Now,
                InstallmentPaymeny = true,
                ClientId = "client3"
            });

            //policy without client
            policies.Add(new Policy()
            {
                Id = "6",
                Email = "99@gmail.com",
                amountInsured = 12,
                InceptionDate = DateTime.Now,
                InstallmentPaymeny = true,
                ClientId = "client99"
            });
            
            clients.Add(new Client()
            {
                Id = "client1",
                Email = "1@gmail.com",
                Name = "Uno",
                Role = "Admin"
            });

            clients.Add(new Client()
            {
                Id = "client2",
                Email = "2@gmail.com",
                Name = "Dos",
                Role = "Admin"
            });

            clients.Add(new Client()
            {
                Id = "client3",
                Email = "3@gmail.com",
                Name = "Tres",
                Role = "User"
            });
;
            clientServicesMock = new Mock<IClientServices>();
            loggerFactoryMock = new Mock<ILoggerFactory>();
            policiesServicesMock = new Mock<IPolicyServices>();
        }

        #region GetClientById


        [Fact]
        /// <summary>
        /// return Ok() when id is valid
        /// </summary>
        public void GetClientById_ReturnsOkResult()
        {
            Client client = this.clients[0];

            clientServicesMock.Setup(m => m.GetClientById(client.Id)).Returns(client);
            
            ClientController clientControllerStub = new ClientController(loggerFactoryMock.Object,
                                                                        clientServicesMock.Object,
                                                                        policiesServicesMock.Object,
                                                                        null
                                                                        );
            
            var response = clientControllerStub.GetById(client.Id);

            Assert.IsType<OkObjectResult>(response);
        }

        [Fact]
        /// <summary>
        /// return not found when id is invalid
        /// </summary>
        public void GetClientById_InvalidName_NotFoundObjectResult()
        {
            Client client = this.clients[0];

            clientServicesMock.Setup(m => m.GetClientById(client.Id)).Throws(new ClientNotFoundException(client.Id));
            
            ClientController clientControllerStub = new ClientController(loggerFactoryMock.Object,
                                                                        clientServicesMock.Object,
                                                                        policiesServicesMock.Object,
                                                                        null
                                                                        );
            
            var response = clientControllerStub.GetById(client.Id);

            Assert.IsType<NotFoundObjectResult>(response);
        }

        [Fact]
        /// <summary>
        /// return not found when id is null
        /// </summary>
        public void GetClientById_NullId_NotFoundObjectResult()
        {
            Client client = this.clients[0];

            clientServicesMock.Setup(m => m.GetClientById(client.Id)).Throws(new ClientFieldRequiredException(client.Id));
            
            ClientController clientControllerStub = new ClientController(loggerFactoryMock.Object,
                                                                        clientServicesMock.Object,
                                                                        policiesServicesMock.Object,
                                                                        null
                                                                        );
            
            var response = clientControllerStub.GetById(client.Id);

            Assert.IsType<NotFoundObjectResult>(response);
        }
        #endregion

        #region GetClientByName
        [Fact]
        /// <summary>
        /// return Ok() when name is valid
        /// </summary>
        public void GetClientByName_ReturnsOkResult()
        {
            Client client = this.clients[0];

            clientServicesMock.Setup(m => m.GetClientByName(client.Name)).Returns(client);
            
            ClientController clientControllerStub = new ClientController(loggerFactoryMock.Object,
                                                                        clientServicesMock.Object,
                                                                        policiesServicesMock.Object,
                                                                        null
                                                                        );
            
            var response = clientControllerStub.GetByName(client.Name);

            Assert.IsType<OkObjectResult>(response);
        }

        [Fact]
        /// <summary>
        /// return not found when name is invalid
        /// </summary>
        public void GetClientByName_InvalidName_NotFoundObjectResult()
        {
            Client client = this.clients[0];

            clientServicesMock.Setup(m => m.GetClientByName(client.Name)).Throws(new ClientNotFoundException(client.Id));
            
            ClientController clientControllerStub = new ClientController(loggerFactoryMock.Object,
                                                                        clientServicesMock.Object,
                                                                        policiesServicesMock.Object,
                                                                        null
                                                                        );
            
            var response = clientControllerStub.GetByName(client.Name);

            Assert.IsType<NotFoundObjectResult>(response);
        }

        [Fact]
        /// <summary>
        /// return not found when name is null
        /// </summary>
        public void GetClientByName_NullId_NotFoundObjectResult()
        {
            Client client = this.clients[0];

            clientServicesMock.Setup(m => m.GetClientByName(client.Name)).Throws(new ClientFieldRequiredException(client.Id));
            
            ClientController clientControllerStub = new ClientController(loggerFactoryMock.Object,
                                                                        clientServicesMock.Object,
                                                                        policiesServicesMock.Object,
                                                                        null
                                                                        );
            
            var response = clientControllerStub.GetByName(client.Name);

            Assert.IsType<NotFoundObjectResult>(response);
        }
        #endregion
    }
}
