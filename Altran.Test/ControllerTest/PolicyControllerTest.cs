using System;
using System.Collections.Generic;
using System.IO;
using Altran.BusinessServices;
using Altran.Controllers;
using Altran.Core;
using Altran.Core.Exception;
using Altran.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace Altran.Test.BusinessServicesTest
{
    public class PolicyControllerTest
    {
        #region props
        private List<Client> clients = new List<Client>();
        private List<Policy> policies = new List<Policy>();
        private readonly PolicyController controller;
        private Mock<IClientServices> clientServicesMock {get;}
        private Mock<IPolicyServices> policiesServicesMock {get;}
        private Mock<ILoggerFactory> loggerFactoryMock {get;}
        #endregion props

        public PolicyControllerTest()
        {
            policies.Add(new Policy()
            {
                Id = "1",
                Email = "1@gmail.com",
                amountInsured = 12,
                InceptionDate = DateTime.Now,
                InstallmentPaymeny = true,
                ClientId = "client1"
            });

            policies.Add(new Policy()
            {
                Id = "2",
                Email = "1@gmail.com",
                amountInsured = 12,
                InceptionDate = DateTime.Now,
                InstallmentPaymeny = true,
                ClientId = "client1"
            });

            policies.Add(new Policy()
            {
                Id = "3",
                Email = "2@gmail.com",
                amountInsured = 12,
                InceptionDate = DateTime.Now,
                InstallmentPaymeny = true,
                ClientId = "client2"
            });

            policies.Add(new Policy()
            {
                Id = "4",
                Email = "2@gmail.com",
                amountInsured = 12,
                InceptionDate = DateTime.Now,
                InstallmentPaymeny = true,
                ClientId = "client2"
            });

            policies.Add(new Policy()
            {
                Id = "5",
                Email = "3@gmail.com",
                amountInsured = 12,
                InceptionDate = DateTime.Now,
                InstallmentPaymeny = true,
                ClientId = "client3"
            });

            //policy without client
            policies.Add(new Policy()
            {
                Id = "6",
                Email = "99@gmail.com",
                amountInsured = 12,
                InceptionDate = DateTime.Now,
                InstallmentPaymeny = true,
                ClientId = "client99"
            });
            
            clients.Add(new Client()
            {
                Id = "client1",
                Email = "1@gmail.com",
                Name = "Uno",
                Role = "Admin"
            });

            clients.Add(new Client()
            {
                Id = "client2",
                Email = "2@gmail.com",
                Name = "Dos",
                Role = "Admin"
            });

            clients.Add(new Client()
            {
                Id = "client3",
                Email = "3@gmail.com",
                Name = "Tres",
                Role = "User"
            });

            policiesServicesMock = new Mock<IPolicyServices>();
            clientServicesMock = new Mock<IClientServices>();
            loggerFactoryMock = new Mock<ILoggerFactory>();
        }

        [Fact]
        /// <summary>
        /// /// validate return code 200
        /// </summary>
        public void GetPolityAllByUserName_ReturnsOkResult()
        {
            //seteo el mock
            Policy policy = this.policies[0];
            List<Policy> policiesMock = this.policies.FindAll(pol => pol.ClientId.Equals(policy.ClientId));
            Client client = this.clients[0];

            clientServicesMock.Setup(m => m.GetClientByName(policy.ClientId)).Returns(client);
            policiesServicesMock.Setup(m => m.GetPoliciesByClientId(client.Id)).Returns(policiesMock);

            PolicyController policyControllerStub = new PolicyController(loggerFactoryMock.Object, 
                                                                        clientServicesMock.Object, 
                                                                        policiesServicesMock.Object, 
                                                                        null);
            var response = policyControllerStub.GetAllByUserName(client.Id);

            Assert.IsType<OkObjectResult>(response);
            
        }

        [Fact]
        /// <summary>
        /// return StatusCode 404 when client not found
        /// </summary>
        public void GetPolityAllByUserName_ClientNotFoundException_ReturnsNotFound()
        {
           //seteo el mock
            Client client = this.clients[0];

            clientServicesMock.Setup(m => m.GetClientByName(client.Id)).Throws(new ClientNotFoundException(client.Id));

            //stub
            PolicyController policyControllerStub = new PolicyController(loggerFactoryMock.Object, 
                                                                        clientServicesMock.Object, 
                                                                        policiesServicesMock.Object, 
                                                                        null);
            var response = policyControllerStub.GetAllByUserName(client.Id);

            //assert
            Assert.True((response as ObjectResult).StatusCode == 404);
        }

        [Fact]
        /// <summary>
        /// validate valid name but invalid policy, return not found
        /// </summary>
        public void GetPolityAllByUserName_ValidNameInvalidPolicy_NotFoundObjectResult()
        {
            //seteo el mock
            Client client = this.clients[0];

            clientServicesMock.Setup(m => m.GetClientByName(client.Id)).Returns(client);
            policiesServicesMock.Setup(m => m.GetPoliciesByClientId(client.Id)).Throws(new PolicyNotFoundException(client.Id, "clientId"));
            //stub
            PolicyController policyControllerStub = new PolicyController(loggerFactoryMock.Object, 
                                                                        clientServicesMock.Object, 
                                                                        policiesServicesMock.Object, 
                                                                        null);
            var response = policyControllerStub.GetAllByUserName(client.Id);

            //assert
            Assert.True((response as ObjectResult).StatusCode == 404);
        }

        [Fact]
        /// <summary>
        /// validate null user name
        /// </summary>
        public void GetPolityAllByUserName_NameNull_NotFoundObjectResult()
        {
            //seteo el mock
            Client client = this.clients[0];

            clientServicesMock.Setup(m => m.GetClientByName(client.Id)).Throws(new ClientFieldRequiredException("clientId"));

            //stub
            PolicyController policyControllerStub = new PolicyController(loggerFactoryMock.Object, 
                                                                        clientServicesMock.Object, 
                                                                        policiesServicesMock.Object, 
                                                                        null);
            var response = policyControllerStub.GetAllByUserName(client.Id);

            //assert
            Assert.True((response as ObjectResult).StatusCode == 404);
        }
    }
}
