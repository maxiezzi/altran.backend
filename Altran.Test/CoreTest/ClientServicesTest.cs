using System.Collections.Generic;
using System.IO;
using Altran.BusinessServices;
using Altran.Core;
using Altran.Core.Exception;
using Altran.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace Altran.Test.CoreTest
{
    public class ClientServicesTest
    {   

        private List<Client> clients = new List<Client>();
        private Mock<ILoggerFactory> loggerFactoryMock {get;}
        private Mock<IApiEnsureServices> apiEnsureServicesMock {get;}

        public ClientServicesTest()
        {
            clients.Add(new Client()
            {
                Id = "client1",
                Email = "1@gmail.com",
                Name = "Uno",
                Role = "Admin"
            });

            clients.Add(new Client()
            {
                Id = "client2",
                Email = "2@gmail.com",
                Name = "Dos",
                Role = "Admin"
            });

            clients.Add(new Client()
            {
                Id = "client3",
                Email = "3@gmail.com",
                Name = "Tres",
                Role = "User"
            });

            apiEnsureServicesMock = new Mock<IApiEnsureServices>();
            loggerFactoryMock = new Mock<ILoggerFactory>();
        }
        /// <summary>
        /// Get Client by Id Exists
        /// </summary>
        [Fact]
        public void GetClientById_Exists()
        {
            //seteo el mock
            apiEnsureServicesMock.Setup(m => m.GetClients()).Returns(this.clients);

            var stub = new ClientServices(apiEnsureServicesMock.Object, loggerFactoryMock.Object, null);
            //Act
            Client client = stub.GetClientById(this.clients[0].Id);
            Assert.Equal(client.Id, this.clients[0].Id);
        }

        /// <summary>
        /// Get Client by Id not Exists
        /// </summary>
        [Fact]
        public void GetClientById_NotExists()
        {
            //seteo el mock
            apiEnsureServicesMock.Setup(m => m.GetClients()).Throws(new ClientNotFoundException("Prueba"));

            var stub = new ClientServices(apiEnsureServicesMock.Object, loggerFactoryMock.Object, null);
            //Act
            Assert.Throws<ClientNotFoundException>(() => stub.GetClientById("Prueba"));
        }

        /// <summary>
        /// Get Client by Id is required
        /// </summary>
        [Fact]
        public void GetClientById_IdIsRequired()
        {
            var stub = new ClientServices(apiEnsureServicesMock.Object, loggerFactoryMock.Object, null);
            //Act
            Assert.Throws<ClientFieldRequiredException>(() => stub.GetClientById(null));
        }

        /// <summary>
        /// Get Client by Id Exists
        /// </summary>
        [Fact]
        public void GetClientByEmail_Exists()
        {
            //seteo el mock
            apiEnsureServicesMock.Setup(m => m.GetClients()).Returns(this.clients);

            var stub = new ClientServices(apiEnsureServicesMock.Object, loggerFactoryMock.Object, null);
            //Act
            Client client = stub.GetClientByEmail(this.clients[0].Email);
            Assert.Equal(client.Email, this.clients[0].Email);
        }

        /// <summary>
        /// Get Client by Id not Exists
        /// </summary>
        [Fact]
        public void GetClientByEmail_NotExists()
        {
            //seteo el mock
            apiEnsureServicesMock.Setup(m => m.GetClients()).Throws(new ClientNotFoundException("Prueba"));

            var stub = new ClientServices(apiEnsureServicesMock.Object, loggerFactoryMock.Object, null);
            //Act
            Assert.Throws<ClientNotFoundException>(() => stub.GetClientByEmail("Prueba"));
        }

        /// <summary>
        /// Get Client by enmail is requred
        /// </summary>
        [Fact]
        public void GetClientByEmail_IdIsRequired()
        {
            var stub = new ClientServices(apiEnsureServicesMock.Object, loggerFactoryMock.Object, null);
            //Act
            Assert.Throws<ClientFieldRequiredException>(() => stub.GetClientByEmail(null));
        }

        /// <summary>
        /// Get Client by NAme Exists
        /// </summary>
        [Fact]
        public void GetClientByName_Exists()
        {
            //seteo el mock
            apiEnsureServicesMock.Setup(m => m.GetClients()).Returns(this.clients);

            var stub = new ClientServices(apiEnsureServicesMock.Object, loggerFactoryMock.Object, null);
            //Act
            Client client = stub.GetClientByName(this.clients[0].Name);
            Assert.Equal(client.Name, this.clients[0].Name);
        }

        /// <summary>
        /// Get Client by Id Name no Exists
        /// </summary>
        [Fact]
        public void GetClientByName_NotExists()
        {
            //seteo el mock
            apiEnsureServicesMock.Setup(m => m.GetClients()).Throws(new ClientNotFoundException("Prueba"));

            var stub = new ClientServices(apiEnsureServicesMock.Object, loggerFactoryMock.Object, null);
            //Act
            Assert.Throws<ClientNotFoundException>(() => stub.GetClientByName("Prueba"));
        }

        /// <summary>
        /// Get Client by name is requred
        /// </summary>
        [Fact]
        public void GetClientByName_IdIsRequired()
        {
            var stub = new ClientServices(apiEnsureServicesMock.Object, loggerFactoryMock.Object, null);
            //Act
            Assert.Throws<ClientFieldRequiredException>(() => stub.GetClientByName(null));
        }
    
        /// <summary>
        /// Get Client by Id Exists
        /// </summary>
        [Fact]
        public void ApiEnsureFailed()
        {
            //seteo el mock
            apiEnsureServicesMock.Setup(m => m.GetClients()).Throws(new ApiEnsureException());

            var stub = new ClientServices(apiEnsureServicesMock.Object, loggerFactoryMock.Object, null);
            //Act
            Assert.Throws<ApiEnsureException>(() => stub.GetClientById(this.clients[0].Id));
        }
    }
}
