using System;
using System.Collections.Generic;
using System.IO;
using Altran.BusinessServices;
using Altran.Core;
using Altran.Core.Exception;
using Altran.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using Xunit;

namespace Altran.Test.CoreTest
{
    public class PolicyServicesTest
    {   

        private List<Policy> Policies = new List<Policy>();
        private Mock<ILoggerFactory> loggerFactoryMock {get;}
        private Mock<IApiEnsureServices> apiEnsureServicesMock {get;}

        public PolicyServicesTest()
        {
            Policies.Add(new Policy()
            {
                Id = "1",
                Email = "1@gmail.com",
                amountInsured = 12,
                InceptionDate = DateTime.Now,
                InstallmentPaymeny = true,
                ClientId = "client1"
            });

            Policies.Add(new Policy()
            {
                Id = "2",
                Email = "1@gmail.com",
                amountInsured = 12,
                InceptionDate = DateTime.Now,
                InstallmentPaymeny = true,
                ClientId = "client1"
            });

            Policies.Add(new Policy()
            {
                Id = "3",
                Email = "2@gmail.com",
                amountInsured = 12,
                InceptionDate = DateTime.Now,
                InstallmentPaymeny = true,
                ClientId = "client2"
            });

            Policies.Add(new Policy()
            {
                Id = "4",
                Email = "2@gmail.com",
                amountInsured = 12,
                InceptionDate = DateTime.Now,
                InstallmentPaymeny = true,
                ClientId = "client2"
            });

            Policies.Add(new Policy()
            {
                Id = "5",
                Email = "3@gmail.com",
                amountInsured = 12,
                InceptionDate = DateTime.Now,
                InstallmentPaymeny = true,
                ClientId = "client3"
            });

            //policy without client
            Policies.Add(new Policy()
            {
                Id = "6",
                Email = "99@gmail.com",
                amountInsured = 12,
                InceptionDate = DateTime.Now,
                InstallmentPaymeny = true,
                ClientId = "client99"
            });

            apiEnsureServicesMock = new Mock<IApiEnsureServices>();
            loggerFactoryMock = new Mock<ILoggerFactory>();
        }

        /// <summary>
        /// Get policy by Id Exists
        /// </summary>
        [Fact]
        public void GetPolicyById_Exists()
        {
            //seteo el mock
            apiEnsureServicesMock.Setup(m => m.GetPolicies()).Returns(this.Policies);

            var stub = new PolicyServices(apiEnsureServicesMock.Object, loggerFactoryMock.Object, null);
            //Act
            Policy policy = stub.GetPolicyById(this.Policies[0].Id);
            Assert.Equal(policy.Id, this.Policies[0].Id);
        }

        /// <summary>
        /// Get policy by Id not Exists
        /// </summary>
        [Fact]
        public void GetPolicyById_NotExists()
        {
            //seteo el mock
            apiEnsureServicesMock.Setup(m => m.GetPolicies()).Throws(new PolicyNotFoundException("","Prueba"));

            var stub = new PolicyServices(apiEnsureServicesMock.Object, loggerFactoryMock.Object, null);
            //Act
            Assert.Throws<PolicyNotFoundException>(() => stub.GetPolicyById("Prueba"));
        }

        /// <summary>
        /// Get policy by Id is required
        /// </summary>
        [Fact]
        public void GetPolicyById_IdIsRequired()
        {
            var stub = new PolicyServices(apiEnsureServicesMock.Object, loggerFactoryMock.Object, null);
            //Act
            Assert.Throws<PolicyFieldRequiredException>(() => stub.GetPolicyById(null));
        }

        /// <summary>
        /// Get Client by Id Exists
        /// </summary>
        [Fact]
        public void GetPoliciesByClientId_Exists()
        {
            //seteo el mock
            apiEnsureServicesMock.Setup(m => m.GetPolicies()).Returns(this.Policies);

            var stub = new PolicyServices(apiEnsureServicesMock.Object, loggerFactoryMock.Object, null);
            //Act
            string clientId = this.Policies[0].ClientId;
            List<Policy> totalPolicy = this.Policies.FindAll(x => x.ClientId.Equals(clientId));
            List<Policy> policy = stub.GetPoliciesByClientId(clientId);
            Assert.Equal(policy.Count, totalPolicy.Count);
        }

        /// <summary>
        /// Get Client by Id not Exists
        /// </summary>
        [Fact]
        public void GetPoliciesByClientId_NotExists()
        {
            //seteo el mock
            apiEnsureServicesMock.Setup(m => m.GetPolicies()).Throws(new PolicyNotFoundException("","Prueba"));

            var stub = new PolicyServices(apiEnsureServicesMock.Object, loggerFactoryMock.Object, null);
            //Act
            Assert.Throws<PolicyNotFoundException>(() => stub.GetPoliciesByClientId("Prueba"));
        }

        /// <summary>
        /// Get Client by enmail is requred
        /// </summary>
        [Fact]
        public void GetPoliciesByClientId_IdIsRequired()
        {
            var stub = new PolicyServices(apiEnsureServicesMock.Object, loggerFactoryMock.Object, null);
            //Act
            Assert.Throws<PolicyFieldRequiredException>(() => stub.GetPoliciesByClientId(null));
        }
    }
}
